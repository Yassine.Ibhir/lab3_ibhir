from datetime import datetime, timedelta
from django.core.validators import ValidationError
import re
from django.db import models


# Yassine Ibhir
# helper method that validates email with regex
def validateEmail(email):
    is_valid = re.search("^*@dawsoncollege.qc.ca$", email)

    if not is_valid:
        raise ValidationError("please eneter a valid dawson college email")
    else:
        return email


# Yassine Ibhir
# Responsible model
class TheResponsible(models.Model):
    first_name = models.CharField(max_length=120, blank=False, null=False)
    last_name = models.CharField(max_length=120, blank=False, null=False)
    email = models.EmailField(null=True, validators=[validateEmail])

    def __str__(self):
        return self.email


# Yassine Ibhir
# Task model has a responsible field as a foreign key
class TheTask(models.Model):
    responsible = models.ForeignKey(TheResponsible, on_delete=models.CASCADE)
    taskName = models.CharField(max_length=120)
    taskDesc = models.CharField(max_length=120)
    start_date = models.DateField(default=datetime.now)
    end_date = models.DateField(default=datetime.now() + timedelta(days=1))

    def __str__(self):
        return self.taskName
