# views for djangoRestFullApiFramework
from django.http import Http404
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import TheTask
from .task_serializer import TaskSerializer

# This function sends path urls to view the Api
@api_view(['GET'])
def task_api_overview(request):
    task_api_urls = {
        'List': 'api/task_list/',
        'Detail': 'api/<int:pk>/task_detail',
    }
    return Response(task_api_urls)

# This function gets all the tasks and
# serializes them by calling the TaskSerializer
# in task_serializer module
@api_view(['GET'])
def api_task_list(request):
    all_tasks = TheTask.objects.all()
    serializer_obj = TaskSerializer(all_tasks, many=True)
    return Response(serializer_obj.data)

# same thing for task details except that
#  here we pass in a pk for the task to view
@api_view(['GET'])
def api_task_detail(request, pk):
    try:
        task_detail = TheTask.objects.get(id=pk)
        serializer_obj = TaskSerializer(instance=task_detail, many=False)
        return Response(serializer_obj.data)
    except TheTask.DoesNotExist:
        raise Http404
